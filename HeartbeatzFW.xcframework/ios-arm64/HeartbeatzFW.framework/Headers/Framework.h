//
//  Framework.h
//  Framework
//
//  Created by Steffin Fox Griswold on 2/24/21.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

//! Project version number for Framework.
FOUNDATION_EXPORT double FrameworkVersionNumber;

//! Project version string for iOS_Framework.
FOUNDATION_EXPORT const unsigned char FrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <iOS_Framework/PublicHeader.h>


