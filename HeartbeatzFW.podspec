Pod::Spec.new do |s|
    s.name         = "HeartbeatzFW"
    s.version      = "1.9.5.2"
    s.summary      = "Framework for communicating with Heartbeaz Devices or Android Library"
    s.description  = <<-DESC
    Framework for communicating with Heartbeaz Devices or Android Library
    DESC
    s.homepage     = "https://npe-inc.com"
    s.license      = "MIT"
    s.author       = { "NPE" => "info@npe-inc.com" }
    s.source       = { :git => "https://bitbucket.org/NpeInc/heartbeatzfw.git", :tag => "#{s.version}" }
    s.vendored_frameworks = "HeartbeatzFW.xcframework"
    s.platforms = { :ios => "13.0", :watchos => "7.0", :tvos => "9.0" }
    s.swift_version = "5"
    s.ios.deployment_target  = "13.0"
    s.watchos.deployment_target  = "7.0"
    s.tvos.deployment_target  = "9.0"
end