# Heartbeatz Framework

### Current Version: 1.9.5.1

### Overview
The following xcFramework and demo application showcases the features and data endpoints available for the Heartbeatz interface with the use of Heartbeatz Android Library.

This code collection enables the developer to connect to Apple Watch via the Heartbeatz interfaces delivered by North Pole Engineering.

Requirements:
- iOS 13+ / watchOS 6+
- Xcode 12+
- Swift 5+
- Physical Apple Watch and iPhone for testing

Control Methods:
- SwiftUI @published objects
- Delegate Methods
- Notification Methods


### Build dependancies
* Xcode 12.5 and above
* Cocoapods 1.0 and above
* Physical iOS 13.0 & above device with Bluetooth to test (Note: Simulator does not have a BLE radio, and can not be used to test communication)
* Heartbeatz Device or Library


### PodFile
```
platform :ios, '13.0'

target 'YourAppNameHere' do
  use_frameworks!

  pod 'HeartbeatzFW', :git => 'https://bitbucket.org/NpeInc/heartbeatzfw.git'
end

post_install do |installer|
    installer.pods_project.build_configurations.each do |config|
        config.build_settings.delete('CODE_SIGNING_ALLOWED')
        config.build_settings.delete('CODE_SIGNING_REQUIRED')
    end
end
```
After modifying the podfile. Open Terminal and type the following commands into Terminal.

* __cd__ /User/You/Development/Path/ (Note: you can drag and drop the podfile into terminal to generate the path. You will just need to remove the `/podfile` from the path after it appears)

* pod install

# Framework Protocols:
**ConnectionModel Delegates**
```
    func didUpdateMetrics()
    func didUpdateSessionState(new: SessionState, prev: SessionState)
    func didUpdateWorkoutState(new: WorkoutSessionState, prev: WorkoutSessionState)
    func didCreateSummary(summary: SummaryObject)
    
    func didReceiveMessage(message: String, key: Int, state: LongMessageState, error: LongMessageStateError?)
    func didSendMessage(state: LongMessageState, error: LongMessageStateError?)
    
    func didUpdateHeartbeatzBLEState(state: HeartbeatzBLEState)

```
**MultiDevice Delegate**
```
    func didDiscoverDevices(availableList: [UserStoredDevices],
                            savedList: [UserStoredDevices],
                            discoveredList: [UserStoredDevices],
                            current: UserStoredDevices?)


```

**Setting HealthKit Read and Write Permissions**

```
    let npePermissions = HBTZPermissions.shared
    npePermissions.HKPremissionsWrite = [.workoutInfo, .distanceCycle, .distanceTreadmill]

    npePermissions.HKPremissionsRead = [.workoutInfo, .heartrate, .activeEnergy, .basilEnergy, .distanceCycle, .distanceTreadmill]

```

**HealthKit Permission Callbacks**
```
    setupHealthKit() { success in }
    requestPermissions() { success, error in }
    queryPermissionStatus() { success, errors in }

```


**Bluetooth Permission Callbacks**
```
    checkBLEPermissions { success in }

```




# ConnectionModel Vars & Functions

**INFORMATIONAL**
```
    var currentFrameworksVersion: String
    var currentFirmwareVersion: String
    var uniqueDeviceId: [UInt8]?
    var currentVersion: String
    var uniqueSessionId: UInt16
```


**DELEGATE AND INSTANCE**
```
    var delegate: ConnectionModelDelegate?
    static let sharedConnectionModel = ConnectionModel() // Legacy
```

**HEARTBEATZ DEVICE**
```
    var heartbeatzBLEState: HeartbeatzBLEState = .unknown

    enum HeartbeatzBLEState {
        case disconnected
        case connecting
        case connected
        case unknown
    }
```


**CONTROL FLAGS**
```
    var autoConnectModel = true
    var useHaptic: HapticControl = .all
    var disableScanning = false
    var keepConnectionInIdle = true
    var showCaloriesDuringIdle = true
    var showCommsPrint = false
    var showDebugPrint = false
    var shouldUseWatchConnectionComms = false
    var watchFaceDimmedWorkoutActive = false
    var pauseWorkoutOnDisconnect = false
    var useMultiDevice: Bool = false
    var useDelegateMethod = false
```

**HEALTHKIT**
```
    let healthStore = HKHealthStore()
    let configuration = HKWorkoutConfiguration()
    var session: HKWorkoutSession!
    var builder: HKLiveWorkoutBuilder!
    @Published var lastSession: HKWorkoutSession!
```


**PRODUCT**
```
    var lockerInst: LockerDevice
    var waspInst: WaspBroadcastDevice
    @Published var deviceName: String?
```

**SESSION STATES**
```
    @Published var sessionState: SessionState = .none
    @Published var workoutSessionState: WorkoutSessionState = .none
```



**CONFIGURE**
```
    var minRssi: Int8 = -80
    var minProxyRssi: Int8 = -40
    var androidLibrary: AndroidTXShunt = .none
    var scanForDeviceTypes: [AweProductType]
    
```


**WORKOUT RELATED**
```
    @Published var workoutElements: WorkoutElements
    @Published var activitySummary: HKActivitySummary?
    @Published var equipmentTime: TimeInterval?

    @Published var workoutTypes: [WorkoutType]
    @Published var workoutType: WorkoutType?
    
    var phoneCurrHr: Measurement<UnitCadence>?
    var activeCalories: Measurement<UnitEnergy>?
    var totalCalories: Measurement<UnitEnergy>?
    var deviceTotalCalories: Measurement<UnitEnergy>?
    var totalDistance: Measurement<UnitLength>?
    var cadence: Measurement<UnitCadence>?
    var currAvgHr: Measurement<UnitCadence>?
    var avgCadence: Measurement<UnitCadence>?
    var avgBikeCadence: Measurement<UnitCadence>?
    var avgPower: Measurement<UnitPower>?
    var avgSpeed: Measurement<UnitSpeed>?
    var avgPace: Measurement<UnitDuration>?
    var avgBikeSpeed: Measurement<UnitSpeed>?
    var instantPower: Measurement<UnitPower>?
    var instantSpeed: Measurement<UnitSpeed>?
    var instantPace: Measurement<UnitDuration>?
    var strokeCount: Measurement<UnitCount>?
    var sessionTimeSwift: TimeInterval?

    var pauseInterval:TimeInterval = 0.0

    var currWorkoutStart: Date?
    var prevHrValue: Double = 0

    let heartRateUnit = HKUnit.count().unitDivided(by: HKUnit.minute())

 
```

   
**WORKOUT SUMMARY**
```
    var currentSummary: SummaryObject?
    var sessionSummaries: [SummaryObject] = []
    var onSaveMetaData:[String:Any] = [:]
    var sumTitle: String = ""
    
    struct SummaryObject: Any {
        public var activity: Int = 73
        public var title: String = ""
        public var unitMetric: Bool = true
        public var time: TimeInterval = 0.0
        public var avgHeartRate: Measurement<UnitCadence> = Measurement(value: 0.0, unit: UnitCadence.beatsPerMinute)
        public var activeCalories: Measurement<UnitEnergy> = Measurement(value: 0.0, unit: UnitEnergy.calories)
        public var totalCalories: Measurement<UnitEnergy> = Measurement(value: 0.0, unit: UnitEnergy.calories)
        public var deviceTotalCalories: Measurement<UnitEnergy> = Measurement(value: 0.0, unit: UnitEnergy.calories)
        public var totalDistance: Measurement<UnitLength> = Measurement(value: 0.0, unit: UnitLength.meters)
        public var avgSpeed: Measurement<UnitSpeed> = Measurement(value: 0.0, unit: UnitSpeed.metersPerSecond)
        public var avgPower: Measurement<UnitPower> = Measurement(value: 0.0, unit: UnitPower.watts)
        public var avgBikeCadence: Measurement<UnitCadence> = Measurement(value: 0.0, unit: UnitCadence.revolutionsPerMinute)
        public var avgBikeSpeed: Measurement<UnitSpeed> = Measurement(value: 0.0, unit: UnitSpeed.metersPerSecond)
        public var avgCadence: Measurement<UnitCadence> = Measurement(value: 0.0, unit: UnitCadence.revolutionsPerMinute)
        public var avgPace: Measurement<UnitDuration> = Measurement(value: 0.0, unit: UnitDuration.seconds)
        public var strokeCount: Measurement<UnitCount> = Measurement(value: 0.0, unit: UnitCount.counts)
        public var workoutStart: Date?
        public var workoutEnd: Date?
    }

```

**FUNCTIONS**
``` 
    // Workout Controls
    func configureWorkout(auto: Bool, currentWorkoutType: WorkoutType) -> Bool
    func resumeWorkout() 
    func pauseWorkout()
    func stopWorkout(saveWorkout: Bool, disconnectAfterSave: Bool, metaData: [String:Any]) {
    func cancelWorkout()
    func recoverSession() 

    //For use with Extension Delegate
    func enterActive() {
    func enterInactive()
    func enterBackground()

    // Disconnect from Android Library
    func disconnectHeartbeatz(stopScanning: Bool = true, delay: Double = 15.0) {
    func resetDisconnectHeartbeatz()
    
    //Helpers for Display of Metrics
    func getTimeValue(source: TimeInterval?) -> String
    func getPedalBalanceValue(source: Measurement<UnitPercent>?) -> String
    func getInclineValue(source: Measurement<UnitPercent>?) -> String
    func getCalorieValue(source: Measurement<UnitEnergy>?) -> String
    func getHrValue(source: Measurement<UnitCadence>?) -> String
    func getAscDscValue(source: Measurement<UnitLength>?) -> String
    func getDistanceValue(source: Measurement<UnitLength>?) -> String
    func getPowerValue(source: Measurement<UnitPower>?) -> String
    func getCountValue(source: Measurement<UnitCount>?) -> String
    func getCadenceValue(source: Measurement<UnitCadence>?) -> String
    func getSpeedValue(source: Measurement<UnitSpeed>?) -> String
    func getPaceUnits(source: Measurement<UnitDuration>?) -> String
    func getPaceValue(source: Measurement<UnitDuration>?) -> String

```

**PROTOCOLS**
```
    protocol ConnectionModelDelegate: AnyObject {
        func didUpdateMetrics()
        func didUpdateSessionState(new: SessionState, prev: SessionState)
        func didUpdateWorkoutState(new: WorkoutSessionState, prev: WorkoutSessionState)

        func didCreateSummary(summary: SummaryObject)
    
        func didReceiveMessage(message: String, key: Int, state: LongMessageState, error: LongMessageStateError?)
        func didSendMessage(state: LongMessageState, error: LongMessageStateError?)

        func didUpdateHeartbeatzBLEState(state: HeartbeatzBLEState)

    }
```

**ENUMS**
```
    enum ElementType: UInt8 {
        case none
        case time
        case activeCalories
        case totalCalories
        case power
        case speed
        case pace
        case distance
        case heartrate
        case cadence
        case avgPower
        case avgSpeed
        case avgPace
        case avgHeartrate
        case avgCadence
        case avgBikeCadence
        case avgBikeSpeed
        case ascent
        case descent
        case incline
        case steps
        case blank
    }

    enum SessionState {
        case none
        case initializing
        case initialized
        case idle
        case scanning
        case configure
        case creating
        case starting
        case restoring
        case active
        case paused
        case ending
        case complete
        case discarded
    }
    enum WorkoutSessionState {
        case none
        case idle
        case running
        case paused
        case ended
        case stopped
        case unknown
    }
    enum HapticControl {
        case none
        case warning
        case all
    }
    enum HeartbeatzBLEState {
        case disconnected
        case connecting
        case connected
        case unknown
    }

```

**NOTIFICATIONS**
Note: SessionState and WorkoutSessionState are covered in delegate method. But the whole framework can be set to use Notification observers. 
```
    static let sessionStateEvent = Notification.Name("sessionStateEvent")
    static let longMessageSendEvent = Notification.Name("longMessageSendEvent")
    static let longMessageReceiveEvent = Notification.Name("longMessageReceiveEvent")
    static let communicationsEvent = Notification.Name("communicationsEvent")
    static let workoutSaveEvent = Notification.Name("workoutSaveEvent")
    static let workoutDiscardEvent = Notification.Name("workoutDiscardEvent")
    static let workoutEndEvent = Notification.Name("workoutEndEvent")
    static let workoutStartEvent = Notification.Name("workoutStartEvent")
    static let workoutPauseEvent = Notification.Name("workoutPauseEvent")
    static let workoutResumeEvent = Notification.Name("workoutResumeEvent")
    static let workoutSessionState = Notification.Name("workoutSessionState")
    static let libraryRequestedDisconnect = Notification.Name("libraryRequestedDisconnect")
    static let warnAboutUnknownDisconnect = Notification.Name("warnAboutUnknownDisconnect")
    static let heartbeatzReconnectedSuccess = Notification.Name("heartbeatzReconnectedSuccess")
    static let hbConnectedEvent = Notification.Name("hbConnected")

```


# MultiDevice

**Configure**
```
    var selectedUserStoredDevice: selectedUserStoredDevice?
    var delegate: MultiDeviceDelegate?
    var minRssi: Int8 = -120
    var scanForDeviceTypes: [AweProductType] = [.heartbeatz, .heartbeatzClub, .heartbeatzSeries2, .heartbeatzBcast, .heartbeatzHubb, .heartbeatzRunn, .heartbeatzCable, .heartbeatzLib, .heartbeatzGem]
```

**Functions**
```
    func startListening()
    func saveSelectedDeviceAndStopListening(selected: UserStoredDevices )
    func listUserStoredDevices(scan: Bool)
    func editUserStoredDevices(device: UserStoredDevices?)
    func forgetUserStoredDevices(device: UserStoredDevices?)
    func discconnectUserStoredDevices(device: UserStoredDevices?, forget: Bool)

```

# HBTZPermissions

**Configure**
```
    static let shared = HBTZPermissions()
    var HKPremissionsWrite: [HealthKitPermissions] = [.workoutInfo, .distanceCycle, .distanceTreadmill]
    var HKPremissionsRead: [HealthKitPermissions] = [.workoutInfo, .heartrate, .activeEnergy, .basilEnergy, .distanceCycle, .distanceTreadmill]
    var shownHKPermissions: Bool
    var shownBLEPermissions: Bool
    var secondsToCheckBLEPermissions: Int = 12
```

**Functions**
```
    func setupHealthKit(completion: @escaping (Bool)->())
    func requestPermissions(completion: @escaping (Bool, Error?)->())
    func queryPermissionStatus(completion: @escaping (Bool,[String])->())
    func checkBLEPermissions(completion: @escaping (Bool, CBManagerAuthorization)->()) 

```
